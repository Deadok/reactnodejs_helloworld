const webpack = require('webpack');
const path = require('path');
const NODE_ENV = process.env.NODE_ENV || 'dev';
const BUILD_DIR = path.resolve(__dirname, 'public', 'js')
const APP_DIR = path.resolve(__dirname, 'src');

const config = {
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"]
    },
    entry: {
        app: ["babel-polyfill", path.join(APP_DIR, 'index.jsx')]
    },
    watch: NODE_ENV === 'dev' ? true : false,
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js',
        library: 'reactApp',
        hotUpdateChunkFilename: 'hot/[id].[hash].hot-update.js',
        hotUpdateMainFilename: 'hot/[hash].hot-update.json'
    },
    devtool: NODE_ENV === 'dev' ? 'inline-source-map' : false,
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                enforce: 'pre',
                loader: 'eslint-loader',
                options: {
                    configFile: '.eslintrc',
                    failOnError: true,
                },
                exclude: /node_modules/,
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                include: [
                    path.resolve(__dirname, 'src')
                ],
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'stage-0', 'react'],
                },
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ]
}

//Минифицировать скрипты если продакшн
if (NODE_ENV === "prod") {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}

module.exports = config;