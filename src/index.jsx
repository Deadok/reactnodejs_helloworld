'use strict'
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import App from './app/app'

ReactDOM.render(
    <App />,
    document.getElementById('root')
);

export { React, ReactDOM };
