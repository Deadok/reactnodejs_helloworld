import React, { Component, PropTypes } from 'react';
import Article from './article';

class News extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        data: PropTypes.array.isRequired
    }

    render() {
        var data = this.props.data;
        var length = this.props.data.length;
        var template = length > 0 ? data.map(function (item, index) {
            return (
                    <Article key={index} data={item}/>
                );
        }) : <div>"Нет новостей" </div>;

        return (
            <div className="container">
                <div className="row">
                    <div className={data.length === 0 ? "hidden-xs-up" : "col-lg-12 text-left"}>
                        Всего новостей: {length}
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        {template}
                    </div>
                </div>
            </div>
        );
    }
}

export default News;




