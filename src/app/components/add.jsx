import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

class Input extends Component {
    constructor(props) {
        super(props);
        this.onPostClick = this.onPostClick.bind(this);
        this.onCheckChanged = this.onCheckChanged.bind(this);
    }

    componentDidMount() {
        ReactDOM.findDOMNode(this.refs.postInput).focus();
    }

    onPostClick(e) {
        e.preventDefault();
        if(this.state.acceptRules) {
            const author = ReactDOM.findDOMNode(this.refs.author).value;
            const text = ReactDOM.findDOMNode(this.refs.postInput).value;
            alert(author + "\n" + text);
        }
    }

    onCheckChanged(e) {
        if (e.target.type === "checkbox") {
            this.setState({ acceptRules: e.target.checked })
        }
    }

    state = {
        acceptRules: false
    }

    render() {
        return (
            <form className="border">
                <div className="form-group row">
                    <label htmlFor="guestInput" className="col-form-label col-2"> Автор: </label>
                    <input id="guestInput" className="form-control col-12 control-lg" type="text" />
                </div>
                <div className="form-group row">
                    <label ref="author" htmlFor="textInput" className="col-form-label col-2"> Cообщение: </label>
                    <textarea rows="3" id="textInput" defaultValue=""
                        ref="postInput"
                        cols="150"
                        type="text"
                        className="form-control col-12 control-lg"
                        placeholder="Ваше сообщение"
                        aria-describedby="basic-addon2" />
                </div>
                <div className="form-group row">
                    <button onClick={this.onPostClick}
                        className={
                            this.state.acceptRules ? "form-control btn btn-primary col-5" 
                                : "form-control btn btn-primary disabled col-5" 
                             }> Отправить
                    </button>
                </div>
                <div className="form-group row text-left"> 
                    <label className="form-check-label control-lg col-12">
                        <input onChange={this.onCheckChanged} defaultChecked={this.state.acceptRules} type="checkbox" className="form-check-input" />
                        Согласен с правилами
                    </label>    
                </div>
            </form>
        );
    }
}

export default Input;