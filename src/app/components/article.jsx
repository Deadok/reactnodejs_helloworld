import React, { Component, PropTypes } from 'react';

export default class extends Component {
    constructor(props) {
        super(props);
        this.dislpayBigText = this.dislpayBigText.bind(this);
    }

    static propTypes = {
        data: PropTypes.shape({
            author: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
            bigText: PropTypes.string.isRequired
        })
    }

    dislpayBigText(e) {
        e.preventDefault();
        this.setState((prev) => {
            return {
                visible: !prev.visible
            }
        });
    }

    state = {
        visible: false
    }

    render() {
        var item = this.props.data;
        return (
            <div className="blockquote bg-inverse text-white mb-2 mt-2">
                <div className="row">
                    <div className="col-md-12">
                        <strong>{item.author}: </strong>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        {item.text}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <a href="#" onClick={this.dislpayBigText} className='btn btn-md btn-default'>
                            { this.state.visible ? "Скрыть" : "Подробнее" }
                        </a>
                    </div>
                </div>
                <div className={this.state.visible ? "row rounded mt-2 ml-md-2 mr-md-2 text-primary bg-faded border" : "hidden-xs-up"}>
                    <div className="col-md-12">
                        {item.bigText}
                    </div>
                </div>
            </div>);
    }
}