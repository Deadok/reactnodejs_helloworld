import React, { Component } from 'react';
import News from './components/news';
import Input from './components/add';

var my_news = [
    {
        author: 'Саша Печкин',
        text: 'В четчерг, четвертого числа...',
        bigText: 'в четыре с четвертью часа четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж.'
    },
    {
        author: 'Просто Вася',
        text: 'Считаю, что $ должен стоить 35 рублей!',
        bigText: 'А евро 42!'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        bigText: 'На самом деле платно, просто нужно прочитать очень длинное лицензионное соглашение'
    }
];

export default class extends Component {
    render() {
        return (
            <div className="ml-lg-5 mr-lg-5">
                <div className="container-fluid ">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <h3 className="mb-lg-5"> ПРИВЕТ <small className="text-muted">REACT </small></h3>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <News data={my_news} />
                        </div>
                    </div>
                    <div className="row jumbotron ml-3 mr-3">
                        <div className="col-md-12">
                            <Input />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
