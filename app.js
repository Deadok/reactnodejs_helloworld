const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
//Порт приложения для отладки
const appPort = 5000;
const app = express();
//Подключить роутер
const staticRouter = require('./app/router');
const apiRouter = require('./app/api-router');
app.set("port", process.env.PORT || appPort);
app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/public/html"));
app.use(express.static(__dirname + "/public/js"));
app.use(express.static(__dirname + "/public/css"));

//Заюзать роутер для статики
app.use('/', staticRouter);
app.use('/api/v1/', apiRouter);

app.listen(app.get("port"), () => {
    console.info("Приложение запущено на порту " + app.get("port"));
});